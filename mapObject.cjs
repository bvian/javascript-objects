function mapObject(obj, cb) {
    if (typeof obj !== 'object' || typeof cb !== 'function') return [];

    const newObj = {};
    for (let key in obj) {
        newObj[key] = cb(key, obj[key]);
    }
    return JSON.stringify(newObj) === '{}'? [] : newObj;
}

module.exports = mapObject;