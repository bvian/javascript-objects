function invert(obj) {
    if (typeof obj !== 'object') return [];

    const newObj = {};
    
    for (let key in obj) {
        const newKey = obj[key];
        const newValue = key;
        newObj[newKey] = newValue;
    }

    return JSON.stringify(newObj) === '{}'? [] : newObj;
}

module.exports = invert;