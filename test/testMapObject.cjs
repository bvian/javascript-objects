const { testObject } = require('./testObject.cjs');
const mapObject = require('../mapObject.cjs');

const cb = (key, val) => {
    if (typeof val === 'string') {
        val = val + ' edited';
    }
    else {
        val = val + 5;
    }
    return val;
}

const result = mapObject(testObject, cb);
console.log(result);