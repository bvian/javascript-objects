function pairs(obj) {
    if (typeof obj !== 'object') return [];

    const pairsList = [];
    
    for (let key in obj) {
        pairsList.push([key, obj[key]]);
    }

    return pairsList;
}

module.exports = pairs;